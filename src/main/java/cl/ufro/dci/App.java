package cl.ufro.dci;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Map<String, String[]> datos = getDatos();
        sortdoc(datos);
    }

    public static Map<String, String[]> getDatos() {
        //Obtener datos del link
        final String url = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";
        Document doc;
        try {
            doc = Jsoup.connect(url).get();
        } catch (Exception e) {
            System.out.println("Error de conexion");
            doc = null;
        }
        Element link = doc.getElementById("gr");
        Map<String, String[]> datosMap = new HashMap<String, String[]>();
        Elements encabezadosData = link.select("th");
        String[] encabezados = encabezadosData.text().split(" ");
        //Ordenardos dentro de un Map por mes
        for (int c = 1; c < encabezados.length; c++) {
            String[] aux = new String[31];
            int countAux = 0;
            for (int i = 2; i <= 32; i++) {
                if (i < 10) {
                    String valUFDiaString = link.getElementById("gr_ctl0" + i + "_" + encabezados[c]).text();
                    aux[countAux] = valUFDiaString;
                } else {
                    String valUFDiaString = link.getElementById("gr_ctl" + i + "_" + encabezados[c]).text();
                    aux[countAux] = valUFDiaString;
                }
                countAux++;
            }
           datosMap.put(encabezados[c], aux);
        }
        return datosMap;
    }

    public static void sortdoc(Map<String, String[]> dat) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese fecha en el formato dia/mes: ");
        String fecha = scan.nextLine();
        String[] fechaArray = fecha.split("/");
        int diaGet = Integer.parseInt(fechaArray[0]);
        int mesGet = Integer.parseInt(fechaArray[1]);
        String[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
        dat.forEach((s, dias) -> {
                    if (s.equals(meses[mesGet - 1])) {
                        if (dias[diaGet - 1].equals("")) {
                            System.out.println("no hay valor para esta fecha");
                        } else
                            System.out.println("Valor UF: " + dias[diaGet - 1]);
                    }
                }
        );
    }

}
